#Futuramoji

A few custom Futurama emoji for Slack. Emoji are designed my [Paperbeatsscissors](http://paperbeatsscissors.com). Emoji upload script by [peterwilli](https://github.com/peterwilli). Original emoji uploader is [here](https://github.com/CodeBuffet/Slekkel).

## Installation

- Clone this repo
- cd to the repo folder
- Run `sudo npm install -g slimerjs` to install the slimerjs binary if you haven't done already
- Run `npm install`
- Run `make run` and follow the instructions
